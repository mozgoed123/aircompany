package Planes;

import models.EClassificationLevel;
import models.EExperimentalTypes;

public class ExperimentalPlane extends Plane {
    private final EExperimentalTypes type;

    private final EClassificationLevel classificationLevel;

    public EClassificationLevel getClassificationLevel() {
        return classificationLevel;
    }

    public ExperimentalPlane(Plane plane, EExperimentalTypes type, EClassificationLevel classificationLevel) {
        super(plane.getModel(), plane.getMaxSpeed(), plane.getMaxFlightDistance(), plane.getMaxLoadCapacity());
        this.type = type;
        this.classificationLevel = classificationLevel;
    }

    @Override
    public String toString() {
        return "experimentalPlane{" +
                "model='" + getModel() + '\'' +
                '}';
    }
}
