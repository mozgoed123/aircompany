package models;

public enum EClassificationLevel {
    CONFIDENTIAL,
    SECRET,
    TOP_SECRET,
    UNCLASSIFIED,
}
