package Planes;

import models.EMilitaryType;

import java.util.Objects;

public class MilitaryPlane extends Plane {
    private final EMilitaryType type;

    public EMilitaryType getType() {
        return type;
    }

    public MilitaryPlane(Plane plane, EMilitaryType type) {
        super(plane.getModel(), plane.getMaxSpeed(), plane.getMaxFlightDistance(), plane.getMaxLoadCapacity());
        this.type = type;
    }

    @Override
    public boolean equals(Object plane) {
        if (this == plane) return true;
        if (!(plane instanceof MilitaryPlane)) return false;
        if (!super.equals(plane)) return false;

        MilitaryPlane militaryPlane = (MilitaryPlane) plane;
        return type == militaryPlane.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), type);
    }

    @Override
    public String toString() {
        return super.toString().replace("}",
                ", type=" + type +
                        '}');
    }
}
