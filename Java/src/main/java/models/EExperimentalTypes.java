package models;

public enum EExperimentalTypes {
    HIGH_ALTITUDE,
    HYPERSONIC,
    LIFTING_BODY,
    VTOL
}
